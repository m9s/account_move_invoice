# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
"account_move_invoice"
from trytond.model import ModelView, ModelSQL, fields

class Move(ModelSQL, ModelView):
    '''Extend class Move with attribute invoice to have a callback
    from a move to the invoice where it was created.'''
    _name = 'account.move'
    invoice = fields.One2Many('account.invoice', 'move', 'Invoice')
Move()

