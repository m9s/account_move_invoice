#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Account Move Invoice',
    'name_de_DE': 'Buchhaltung Buchungssatz Rechnung',
    'version': '2.2.0',
    'author': 'virtual-things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Invoice on accounting move
    - Adds the field invoice to account move as a helper to determine if a move
      is related to an invoice.
''',
    'description_de_DE': '''Rechnung auf Buchungssatz
    - Fügt das Feld Rechnung auf Buchungssatz hinzu (Helferfunktion zur
      Ermittlung, ob ein Buchungssatz mit einer Rechnung verknüpft ist).
''',
    'depends': [
        'account_invoice',
    ],
    'xml': [
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
