account_move_invoice Module
===========================

This module adds the attribute invoice to account move.
It is an ineffective helper to determine if a move is related to an invoice.

It is used in several modules and reports to simplify the detection of a 
relation of a move line to an invoice. Additional it is possible to relate to 
attributes from an invoice while evaluating a move line.



